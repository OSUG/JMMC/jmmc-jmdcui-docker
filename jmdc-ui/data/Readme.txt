sample-01.file          : few records (injest format) with wrong format (before alx generation script fix)
sample-01.fixed.file    : few records (injest format) with valid format
sample-01.noheader.file : few records (injest format) with valid format but header line
sample-02.file          : test data (user format)
sample-03.file          : test data with extra columns (user format)
sample-04.file          : test data with shuffled columns (user format)
sample-05.file          : test data with valid but duplicated records (user format)
sample-06.file          : one record with non breaking space in ID (injest format)
sample-07.file          : one record with invalid starID, bandcode should be K (8)
sample-08.file          : testing bands
sample-09.file          : invalid star ids