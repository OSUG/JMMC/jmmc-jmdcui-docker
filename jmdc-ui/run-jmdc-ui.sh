#!/bin/sh
echo "Run alembic upgrade head for $1"
alembic -c $1 upgrade head
echo "Run pserve using $1"
#exec gosu www-data pserve $1
exec pserve $1
