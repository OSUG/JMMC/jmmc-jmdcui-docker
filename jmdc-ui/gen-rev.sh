#!/bin/bash
. ../env/bin/activate
alembic -c development.ini revision --autogenerate -m "$1"