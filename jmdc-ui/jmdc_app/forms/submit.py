from wtforms import TextAreaField, StringField, FileField
from wtforms import validators
from wtforms.form import Form


class FormSubmitResults(Form):
    email = StringField('Email', [validators.DataRequired()])
    csv_data = TextAreaField('CSV Data', [validators.DataRequired()])

class FormImportCsv(Form):
    email = StringField('Email', [validators.DataRequired()])
    import_file = FileField('CSV File', [validators.DataRequired()])
