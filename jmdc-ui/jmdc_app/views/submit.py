import codecs
import csv
import datetime
import json

import secrets
from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config
from pyramid_mailer.message import Message

from jmdc_app.forms.submit import FormImportCsv, FormSubmitResults
from jmdc_app.models.star_info import StarInfo
from jmdc_app.models.submission import StarInfoEntry, Submission


class import_new_data():

    def __init__(self, context=None, request=None):
        self.context = context
        self.request = request
        self.session = request.dbsession
        self.session.begin_nested()
        self.line_error = None
        self.errors = None
        self.line = 0
        self.header = None
        self.entries = None

    def append_error(self, error):
        if not self.errors:
            self.errors = []
        self.errors.append(error)

    #
    # this imports the data synchronously
    # it only stores the data as a json dump in the database
    # and defers all processing for later
    #

    def import_entry_asynchronous(self, entry):
        print("ASYNC %5d %s"%(self.line, entry))
        if not self.sub.headers:
            hdr = StarInfo.parse_header(entry)
            if hdr:
                print("PARSING HEADER  : %s %s"%(type(hdr), repr(hdr)))
                self.sub.parse_mode = hdr[0]
                self.sub.headers = json.dumps(entry)
                self.header = entry
                print("header row detected, skipping")
                return
            print("not a header")
            return  

        # add line as a star_info_entry
        si_entry = StarInfoEntry()
        si_entry.line_number = self.line
        si_entry.line_data = json.dumps(entry)
        self.sub.star_info_entries.append(si_entry)


    #
    # this imports the data synchronously.
    # the user has to wait forever for the page to return
    # this causes issues when using a front end processor
    # such as haproxy in front of the application.
    # in this case, it's better to use the async subsystem
    # above
    #

    def import_entry_synchronous(self, entry):
        print("SYNC  %5d %s"%(self.line, entry))
        if not self.header:
            print()
            hdr = StarInfo.parse_header(entry)
            if hdr:
                print("PARSING HEADER  :", hdr)
                self.header = entry
                print("header row detected, skipping")
                return None
            # no header detected...
            print("no header found")
            return None

        si = StarInfo(self.session, self.header, entry)
        if si:
            if not self.entries:
                self.entries = []
            self.entries.append({'line': self.line, 'entry': si})

            # TODO check next line of code...
            # is_unique can duplicate errors for non-uniqueness

            # NOTE:
            # this does only check if the entry is not YET in the database.
            # entries for the current batch is NOT checked for uniqueness
            if si.is_unique():
                # check if there is an error for this line
                if not si.errors:
                    si_entry = StarInfoEntry()
                    si_entry.line_number = self.line
                    si_entry.star_info = si
                    self.sub.star_info_entries.append(si_entry)
                else:
                    self.line_error = True
            else:
                self.line_error = True

    def import_entry_set(self, email, entry_set):
        self.sub = Submission()
        dt = datetime.datetime.now()
        print("email   ", email)
        print("datetime", dt)
        self.sub.email = email
        self.sub.create_ts = dt
        self.sub.upload_ticket = secrets.token_urlsafe(32)
        self.sub.moderate_ticket = secrets.token_urlsafe(32)
        run_async = self.request.registry.settings.get('data_ingest.async', False)
        if isinstance(run_async, str):
            run_async = run_async == 'True'
        print(type(run_async))
        try:
            for row in entry_set:
                if not row:
                    break
                if run_async:
                    # async data ingest... skip all data handling
                    self.import_entry_asynchronous(row)
                else:  
                    # not doing async ingest, handle all lines directly
                    self.import_entry_synchronous(row)
                
                self.line += 1
        except UnicodeDecodeError as e:
            self.append_error('invalid character detected on line %d, possible illegal unicode value'%(self.line + 1))
        except Exception as e:
            print('\n')
            import traceback
            traceback.print_exc()
            print(e)
            print('something bad happened on line %d'%(self.line))
            return False
        else:
            # at this point, we should have found a header
            if not self.header:
                print("NO HEADER FOUND WHILE PARSING THE FILE")
                self.append_error("Unable to find line with header information")

            if self.errors or self.line_error:
                print("DEBUG: we had errors")
                print(self.errors)
                return False

            print("DEBUG: add success")
            self.session.add(self.sub)

            # all is well, send mail if we have a sender.email config set ...
            verify_url = self.request.route_url('verify',token=self.sub.upload_ticket)
            moderate_url = self.request.route_url('moderate-submission', token=self.sub.moderate_ticket)
            print("DEBUG: moderation url %s"%(moderate_url))
            try:
                sender = self.request.registry.settings['jmdc.sender.email']
            except:
                print("DEBUG: no mail sent for submission %s url %s"%(self.sub.email, verify_url))
            else:
                print("DEBUG: sending test mail to %s url %s"%(self.sub.email, verify_url))
                # TODO: change the mail subject
                message = Message(subject="Hello, world!", sender=sender,
                                  recipients=[self.sub.email],
                                  body="If you didn't submit any data at JMMC, please ignore this message\n Else  please confirm your submission visiting next url : \n %s \nRegards!"%verify_url) 
                print("DEBUG: sending confirmation mail to %s with url : %s"%(self.sub.email, verify_url))
                subject="JMMC - JMDC, please confirm your submission!"
                body =  "Hi,\n If you didn't submit any data at JMMC, please ignore this message\n"
                body += "Else please confirm your submission visiting next url : \n %s \n" % verify_url  
                body += "You will be contacted by the administrators if some records are not appropriate.\n"
                body += "Thank you for your submission. Best regards!" 
                message = Message(subject=subject, sender=sender, recipients=[self.sub.email], body=body)
                self.request.mailer.send(message)

            self.session.commit()

            return True

    @view_config(route_name='submit_text', renderer='../templates/submit_text.jinja2', require_csrf=True)
    def submit_results(self):
        form = FormSubmitResults(self.request.POST)
        if self.request.POST:
            email = form.email.data
            csv_lines = form.csv_data.data.splitlines()
            print(len(csv_lines), "lines to read")
            csv_data = csv.reader(csv_lines)
            if self.import_entry_set(email, csv_data):
               return HTTPFound(location=self.request.route_path('submit_thanks'))
        return { 'form': form, 'errors': self.errors, 'entries': self.entries  }

    @view_config(route_name='submit_csv', renderer='../templates/submit_csv.jinja2', require_csrf=True)
    def jmdc_home(self):
        form = FormImportCsv(self.request.POST)
        if self.request.POST:
            email = form.email.data
            import_file = self.request.POST[form.import_file.name]
            print(import_file.filename)
            print(import_file.file)
            data_src = import_file.file
            data_src.seek(0)
            reader = csv.reader(codecs.iterdecode(data_src, 'utf-8'))
            if self.import_entry_set(email, reader):
                return HTTPFound(location=self.request.route_path('submit_thanks'))
        return { 'form': form, 'errors': self.errors, 'entries': self.entries }

    @view_config(route_name='submit_thanks', renderer='../templates/submit_thanks.jinja2')
    def submit_thanks(self):
        return {}
