from pyramid.view import view_config

from jmdc_app.models.star_info import StarInfo
from jmdc_app.models.submission import StarInfoEntry


@view_config(route_name='jmdc_catalog', renderer='../templates/jmdc_catalog.jinja2')
def jmdc_home(request):

    session = request.dbsession
    # order by id (creation order  / look during import ) and check for validated ones
    stars = session.query(StarInfo).filter(StarInfo.star_info_entry.any(StarInfoEntry.validated==True)).order_by(
        StarInfo.id).all()

    return { 'stars': stars }