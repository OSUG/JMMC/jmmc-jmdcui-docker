from pyramid.view import view_config

from jmdc_app.models import StarInfo
from collections import OrderedDict

@view_config(route_name='jmdc_doc', renderer='../templates/jmdc_doc.jinja2')
def jmdc_doc(request):

    session = request.dbsession

    fields = StarInfo.FIELDS_DUMP['fields']
    columns=OrderedDict()
    for f in StarInfo.MAINFIELDS:
        columns[f]=fields[f]

    return { "columns": columns }
