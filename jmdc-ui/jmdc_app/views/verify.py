import json
import threading
import traceback
from multiprocessing import Process

import pyramid.httpexceptions as exc
from pyramid.httpexceptions import HTTPFound
from pyramid.renderers import render_to_response
from pyramid.view import view_config
from pyramid_mailer.mailer import Mailer
from pyramid_mailer.message import Message
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from jmdc_app.models.star_info import StarInfo
from jmdc_app.models.submission import StarInfoEntry, Submission


def send_email_to_moderator(settings, moderate_url):
    # send email to moderator
    #moderate_url = request.route_path('moderate-submission', token=submission.moderate_ticket)
    print("DEBUG: moderation url %s"%(moderate_url))
    try:
        sender = settings['jmdc.sender.email']
        moderator = settings['jmdc.moderator.email']
    except:
        print("DEBUG: no mail sent for moderator %s"%(moderate_url))
    else:
        print("DEBUG: sending moderation mail to %s url %s"%(moderator, moderate_url))
        # TODO: change the mail subject
        message = Message(subject="there is one submission to moderate", sender=sender,
                            recipients=[moderator],
                            body="There is a new submission at JMDC, moderation can be done here : \n %s \nRegards!"%moderate_url)
        mailer = Mailer.from_settings(settings)
        mailer.send(message)


class ImportProcess(threading.Thread):
    """
    This thread object launches an external process to take care of importing
    new data.
    we can't pass the submission object here, only the id
    """

    def __init__(self, settings, sub, moderate_url):
        super().__init__()
        self.settings = settings
        self.sub_id = sub.id
        self.sub = None
        self.moderate_url = moderate_url

    def async_import_process(self):
        count = 0
        print("running from %s"%(repr(self)))

        # initialize the sqlalchemy session
        engine = create_engine(self.settings['sqlalchemy.url'])
        Session = sessionmaker(engine)
        session = Session()
        print(session)

        # get the sub from the database
        self.sub = session.query(Submission).filter_by(id=self.sub_id).first()

        # loop in the submissions
        print("submission_id = %d, email = %s"%(self.sub.id, self.sub.email)) 
        print("headers(%d) %4d      : %s"%(self.sub.parse_mode, self.sub.id, self.sub.headers))      
        headers = json.loads(self.sub.headers) 
        entries = session.query(StarInfoEntry).filter_by(submission_id=self.sub.id).order_by(StarInfoEntry.line_number).all()
        errors_present = False

        for entry in entries:
            print("processing %4d-%4d : %s"%(self.sub.id, entry.line_number,entry.line_data))
            line_data = json.loads(entry.line_data)

            # create a session for this star_info 
            i_engine = create_engine(self.settings['sqlalchemy.url'])
            i_Session = sessionmaker(i_engine)
            i_session = i_Session()
            print(i_session)
            i_entry = i_session.query(StarInfoEntry).filter_by(submission_id=self.sub.id,line_number=entry.line_number).first()
            print(i_entry)
            si = StarInfo(i_session, headers, line_data, self.sub_id)
            si_msg = si.all_messages()
            if si_msg and si_msg.get('errors', None) is not None:
                errors_present = True
            i_entry.error_data = json.dumps(si_msg)
            if si_msg:
                print('--------------------------------------------------')
                print(si_msg)
                print('--------------------------------------------------')
            # note: the si is saved even if there's an error here. should we do this ?
            # if we don't set this, then the si is not saved, would prevent bad duplicate entries
            i_entry.star_info = si
            i_entry.processed = True
            print("SETTING ENTRY PROCESSED")
            try:
                i_session.commit()
            except Exception as e:
                # something bad happened: dump info about the line that caused problems
                print("FATAL ERROR in line: %d"%(entry.line_number))
                traceback.print_exc()
                print("Errors: '%s'"%(entry.error_data))
                print("Header: '%s'"%(self.sub.headers))
                print("Data  : '%s'"%(entry.line_data))
                print("------> inner session not saved")
                i_session.rollback()
            else:
                print("INNER SESSION COMMITTED")
            i_session.close()
        print('end of processing')
        self.sub.processed = True
        session.commit()

        # send email to moderator (async run) only if there were no errors in the process above
        if not errors_present:
            send_email_to_moderator(self.settings, self.moderate_url)
        else:
            print("we had errors in the file, not sending mail to moderator")

    def run(self):
        p = Process(target=self.async_import_process)
        p.start()
        p.join()

# this function does what it's name says ;-)

def start_import_process(request, submission):
    print("Start the import process thread")
    moderate_url = request.route_path('moderate-submission', token=submission.moderate_ticket)
    t = ImportProcess(request.registry.settings, submission, moderate_url)
    t.daemon = True
    t.start()


@view_config(route_name='verify', renderer='../templates/verified.jinja2')
def moderate_submission(request):
    """
    This view is launched by the user after uploading by cliking 
    on a link sent in an email.
    this starts the data validation process
    """

    token = request.matchdict["token"]

    run_async = request.registry.settings.get('data_ingest.async', False)
    print("Running async '%s'"%(run_async))
    if isinstance(run_async, str):
        run_async = run_async == 'True'

    session = request.dbsession

    submission = session.query(Submission).filter_by(upload_ticket=token).first()

    if not submission:
        # generate error 403
        raise exc.HTTPForbidden()

    just_verified = False
    if not submission.verified:
        session.begin_nested()
        submission.verified = True
        just_verified = True

    # launch the worker process after the data is committed
    if run_async:
        if not submission.processing:
            submission.processing = True
            start_import_process(request, submission)
        else:
            print("Submission already being processed")
    else:
        # send email to moderator (in case of not running async)
        moderate_url = request.route_path('moderate-submission', token=submission.moderate_ticket)
        send_email_to_moderator(request.registry.settings, moderate_url)
        pass

    if just_verified:    
        session.commit()

    context = {
        'session': session, 
        'submission': submission, 
        'just_verified': just_verified 
    }

    if submission.processed:
        entries = []
        sie = session.query(StarInfoEntry).filter_by(submission_id=submission.id).order_by(StarInfoEntry.line_number).all()
        for e in sie:
            entry = {
                'srcdata': e.line_data,
                'duplicated' : e.is_unique()
            }
            ed = e.error_data
            # print(type(ed), ed)
            #
            # for some reason 
            # error_data.get('errors', None) 
            # doesn't work here... go figure
            if ed:
                error_data = json.loads(ed)
                if error_data:
                    entry.update(error_data)
            # why ? entry['unique'] = True
            problem_fields = []
            for v in entry.values():
                if type(v) is dict:
                    for k in v.keys():
                        if k not in problem_fields:
                            problem_fields.append(k)
                            context['has_problem'] = True
            problem_fields.sort()
            entry['problem_fields'] = problem_fields
            entries.append({'line': e.line_number-1, 'entry': entry})
        context['entries'] = entries

    return context

@view_config(route_name='get_line_form')
def get_line_form(request):
    """
    This view generates the form, and updates the data for one line
    """

    token = request.matchdict["token"]
    line = request.matchdict["line"]

    session = request.dbsession
    submission = session.query(Submission).filter_by(upload_ticket=token).first()
    if not submission:
        raise exc.HTTPForbidden()
    sie = session.query(StarInfoEntry).filter_by(submission_id=submission.id, line_number=line).first()
    if not sie:
        raise exc.HTTPNotFound()

    print(token, sie)
    print(submission.headers)

    context = {
    }

    headers = json.loads(submission.headers)

    if request.method == 'DELETE':
        session.delete(sie.star_info)
        session.delete(sie)
        # mark processing to restart since duplicate errors can change
        # TODO set to True only if submissiion's entries contains duplicates
        submission.processing=True
        renderer = '../templates/line_delete.jinja2'

    if request.method == 'GET':
        renderer = '../templates/line_form.jinja2'
        context['headers'] = headers
        context['values'] = json.loads(sie.line_data)
        print(context)

    if request.method == 'POST':
        renderer = '../templates/submit_errors.jinja2'
        print(request.matchdict)
        line_data = []
        for k in headers:
            value = ''
            if k:
                values = request.POST.getall(k)
                if len(values)==1:
                    value = values[0]
            line_data.append(value)
        sie.line_data = json.dumps(line_data)
        print(line_data)
        si = sie.star_info
        si.session = session
        si.warnings = None
        si.errors = None
        si.problem_fields = None
        si.parse(headers, line_data)

        print(si)
        si_msg = si.all_messages()
        if si_msg and si_msg.get('errors', None) is not None:
            errors_present = True
        sie.error_data = json.dumps(si_msg)
        if si_msg:
            print('--------------------------------------------------')
            print(si_msg)
            print('--------------------------------------------------')

        entries = []
        entry = {
            'srcdata': sie.line_data,
            'unique':  sie.is_unique()
        }
        if si_msg:
            entry.update(si_msg)
        problem_fields = []
        for v in entry.values():
            if type(v) is dict:
                for k in v.keys():
                    if k not in problem_fields:
                        problem_fields.append(k)
        problem_fields.sort()
        entry['problem_fields'] = problem_fields
        entries.append({'line': sie.line_number-1, 'entry': entry})
        context['entries'] = entries
        print(context)

        # should test if there are any lines with errors. if not, do that
        nb_errors = session.query(StarInfoEntry).filter(StarInfoEntry.submission_id==submission.id, StarInfoEntry.error_data.like('%"errors": {%')).count()
        print("%d lines contain errors"%(nb_errors))
        if nb_errors == 0:
            moderate_url = request.route_url('moderate-submission', token=submission.moderate_ticket)
            send_email_to_moderator(request.registry.settings, moderate_url)

    return render_to_response(renderer, context)
