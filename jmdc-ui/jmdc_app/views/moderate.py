import re

from pyramid.view import view_config

from jmdc_app.models.submission import StarInfoEntry, Submission


@view_config(route_name='moderate', renderer='../templates/moderate.jinja2')
def moderate(request):
    session = request.dbsession
    submissions = session.query(Submission).all()
    return { 'submissions': submissions }


@view_config(route_name='moderate-submission', renderer='../templates/moderate-submission.jinja2', require_csrf=True)
def moderate_submission(request):
    token = request.matchdict["token"]

    session = request.dbsession

    #obtain sub_id

    submission = session.query(Submission).filter_by(moderate_ticket=token).first()
    sub_id = submission.id

    if request.POST:
        session.begin_nested()
        print("request to moderate submission %s"%(sub_id))

        # TODO duplicates can still be present : must add an uniq constraints mimic the check_uniq rule
        
        re_valid = re.compile(r'^valid\[(\d+)\]$')
        re_comment = re.compile(r'^comment\[(\d+)\]$')

        # data extraction from the form
        data = {}
        for v in request.POST:
            
            var = None

            m = re_valid.match(v)
            if m:
                index = int(m.groups()[0])
                var = 'valid'
                value = True

            m = re_comment.match(v)
            if m:
                index = int(m.groups()[0])
                var = 'comment'
                value = request.POST[v]
                
            if var:
                v = data.get(index, None)
                if not v:
                    v = { 'valid': False}
                v[var] = value
                data[index] = v

        # apply the new states in the database
        print(data)
        submission.validated=True
        for index in data:
            entry = session.query(StarInfoEntry).filter_by(star_info_id=index).first()
            if entry:
                valid = data[index]['valid']
                comment = data[index]['comment']
                entry.validated = valid
                entry.comment = comment
        
        session.commit()

#    entries = session.query(StarInfoEntry).filter_by(submission_id=sub_id).order_by(line_number).all()
    entries = session.query(StarInfoEntry).filter_by(submission_id=sub_id).order_by(StarInfoEntry.line_number).all()

    return { 'token': token, 'submission': submission, 'entries': entries }
