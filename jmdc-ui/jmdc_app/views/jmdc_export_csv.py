from pyramid.view import view_config

from jmdc_app.models.star_info import StarInfo
from jmdc_app.models.submission import StarInfoEntry

@view_config(route_name='jmdc_export_csv', renderer='../templates/jmdc_export_csv.jinja2')
def jmdc_export_csv(request):

    session = request.dbsession
    request.response.content_type = 'text/csv header'

# Csv spec ask for CRLF endlines https://tools.ietf.org/html/rfc4180
# putting next env in .ini could be a solution but an AssertError is thrown. should we continue like this?
#jinja2.newline_sequence = "\r\n"


    # order by id (creation order  / look during import ) and check for validated ones
    stars = session.query(StarInfo).filter(StarInfo.star_info_entry.any(StarInfoEntry.validated == True)).order_by(
        StarInfo.id).all()

    return { 'csv_headers': StarInfo.csv_headers(), 'stars': stars }
