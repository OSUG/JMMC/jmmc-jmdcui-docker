from pyramid.view import view_config


@view_config(route_name='jmdc_about', renderer='../templates/jmdc_about.jinja2')
def jmdc_about(request):

    return {
        'start_date': request.registry.settings['jmdc.start'],
        'version': request.registry.settings['jmdc.version']
    }
