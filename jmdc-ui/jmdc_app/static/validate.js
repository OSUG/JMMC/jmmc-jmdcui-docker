
function get_line_form(entry_log) {
    let input_forms = entry_log.getElementsByClassName('input-form');
    if (input_forms.length!=0) return;
    let ajax = new XMLHttpRequest()
    ajax.addEventListener("load", (event) => {
        set_line_form(event, entry_log);
    });
    var line_no = entry_log.getElementsByClassName("lineno");
    if (line_no.length != 1) return false;
    line_no = line_no[0];
    line_no = line_no.innerHTML;
    console.log("'"+line_no+"'");
    line_no = parseInt(line_no);
    console.log(line_no);
    ajax.open("GET", document.location+"/"+line_no);
    ajax.send();
}

function set_line_form(event, entry_log) {
    if (event.target.status!=200) {
        console.log("error");
        console.log(event.target);
        return;
    }
    let template = document.createElement("template");
    template.innerHTML = event.target.response.trim();
    let form = template.content.firstChild;
    let buttons = form.getElementsByTagName('button');
    for (var button of buttons) 
        button.addEventListener("click", submit_line_form);
    entry_log.append(form);
}

function submit_line_form(event) {
    event.preventDefault();
    let button = event.target;
    let form = button.form;
    let formdata = new FormData(form);
    formdata.append(button.name, button.value);
    let ajax = new XMLHttpRequest();
    ajax.addEventListener("load", (event) => {
        update_line(event, form);
    });
    let line = form.closest(".entry-log");
    let line_no = parseInt(line.querySelector('.lineno').innerHTML);
    ajax.open('POST', document.location+"/"+line_no);
    ajax.send(formdata);
}

function delete_line(event) {
    event.preventDefault();
    let button = event.target;
    let line = button.closest(".entry-log");
    let line_no = parseInt(line.querySelector('.lineno').innerHTML);

    let do_rm = confirm("Please confirm request to delete enty with line "+line_no);
    if ( do_rm == null || do_rm == "") {
      return ;
    }

    let ajax = new XMLHttpRequest();
    ajax.addEventListener("error", (event) => {
        alert("Sorry, an error occured trying to delete line "+ line_no+"\n"+line_no);
        console.log(event);
    });
    ajax.addEventListener("load", (event) => {
        location.reload();
    });

    ajax.open('DELETE', document.location+"/"+line_no);
    ajax.send();

}

function update_line(event, form) {
    if (event.target.status!=200) {
        console.log("error");
        console.log(event.target);
    }
    let entry = form.closest('.entry-log');
    console.log(entry);
    let template = document.createElement("template");
    template.innerHTML = event.target.response.trim();
    var new_entry = template.content.firstChild;
    new_entry = new_entry.querySelector('.entry-log');
    if ( new_entry ) {
        let error_lines = new_entry.getElementsByClassName("error");
        if (error_lines.length>0) {
            let lineno = new_entry.querySelector('.lineno');
            lineno.classList.add('button');
            lineno.addEventListener("click", edit_line);
            console.log(lineno);
        }
        entry.replaceWith(new_entry);
    }else{
        entry.remove();
    }

}

function edit_line(event) {
    let entry_log = event.target.parentElement.parentElement;
    get_line_form(entry_log);
};

function initialize_form() {
    let lines = document.getElementsByClassName("entry-log");
    for (line of lines) {
        var error_lines = line.getElementsByClassName("error");
        if (error_lines.length>0) {
            var lineno_list = line.getElementsByClassName("lineno");
            if (lineno_list.length==1) {
                lineno = lineno_list[0];
                lineno.classList.add("button");
                lineno.addEventListener("click", edit_line);
            }
        }

        var deletelinebuttons = document.getElementsByClassName("deletelinebutton");
        for ( deletelinebutton of deletelinebuttons ) {
            deletelinebutton.addEventListener("click", delete_line);
        }
    }
};

document.addEventListener("readystatechange", (event) => {
    if (event.target.readyState=="complete") {
        initialize_form();
    }
});