from sqlalchemy import Boolean, Column, ForeignKey, Integer, TIMESTAMP, Text
from sqlalchemy.orm import relationship

from . import StarInfo
from .meta import Base
from .star_info import DUPLICATED_ERROR_MSG


class StarInfoEntry(Base):
    __tablename__ = 'star_info_entry'

    # link to the parent submission
    submission_id   = Column(Integer, ForeignKey('submission.id'), primary_key=True)
    # line number on the original file
    line_number     = Column(Integer, primary_key=True)

    # processing-related
    # contents of the line of data, as sent by the user
    line_data       = Column(Text)
    processed       = Column(Boolean)
    error_data      = Column(Text)
    
    # link to the star_info record where the actual data is stored
    star_info_id    = Column(Integer, ForeignKey('star_info.id'), nullable = True, unique = True, primary_key=False)
    # Simbad OID of the object
    star_id         = Column(Integer, ForeignKey('star.id'))

    # validation data
    # line has been validated by the moderator
    validated       = Column(Boolean)
    # comment entered by the moderator
    comment         = Column(Text)

    submission = relationship("Submission", back_populates='star_info_entries')
    star_info = relationship('StarInfo', back_populates='star_info_entry')
    star = relationship('Star', back_populates='star_info_entries')

    def is_unique(self):
        # print("is_unique(): %s : %s " % (self.error_data, DUPLICATED_ERROR_MSG in self.error_data))
        if self.error_data:
            return DUPLICATED_ERROR_MSG in self.error_data
        else:
            return False


class Submission(Base):
    __tablename__ = 'submission'

    id              = Column(Integer, primary_key=True)
    # when was the submission posted
    create_ts       = Column(TIMESTAMP)
    # email given by the sending user
    email           = Column(Text)
    
    # processing-related
    # headers as entered in the submission file (line 1)
    parse_mode      = Column(Integer)
    headers         = Column(Text)
    processing      = Column(Boolean)
    processed       = Column(Boolean)

    # verification tickets (poster has correctly received the email and clicked on the link)
    upload_ticket   = Column(Text)
    verified        = Column(Boolean)

    # moderation data
    moderate_ticket = Column(Text)
    validated       = Column(Boolean)

    star_info_entries = relationship('StarInfoEntry', back_populates='submission')

    def iso_create_time(self):
#        return self.create_ts.isoformat(sep=' ', timespec='seconds')
        return self.create_ts.isoformat()

    def total_entries(self):
        return len(self.star_info_entries)

    def already_processed(self, session):
        return session.query(StarInfoEntry).filter_by(submission_id=self.id, processed=True).count()
