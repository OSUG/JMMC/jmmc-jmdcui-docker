import re
import sys
import time
import urllib.parse

import requests
from psycopg2.errors import UniqueViolation
from sqlalchemy import (Boolean, Column, ForeignKey, Integer,
                        String)
from sqlalchemy.orm import relationship

from .meta import Base


#
# utility functions
#

def execute_http_request(url, success_func):
    caller_name = sys._getframe().f_back.f_code.co_name
    # print("execute_http_request('%s', %s) called by: '%s'"%(url, repr(success_func), caller_name))
    while True:
        try:
            r = requests.get(url)
        except Exception as e:
            print("%s: exception %s raised while requesting '%s'"%(caller_name, e.__class__.__name__, url))
        else:
            if r.status_code == 200:
                return success_func(r)
            else:
                # try again after some delay
                print("%s: something wrong happened while requesting '%s', status_code %d"%(caller_name, url, r.status_code))
                print(r.text)
        print("%s: try again, waiting for 5 seconds"%(caller_name))
        time.sleep(5)


#
# classes
#

class Catalog(Base):
    __tablename__ = 'catalog'

    cat_id = Column(String, primary_key=True)
    name_format = Column(String)

# this contains cached info about stars from simbad and similar catalogs

class Star(Base):
    __tablename__ = 'star'

    # this ID comes from the CDS, must be set on object creation
    id = Column(Integer, primary_key=True, autoincrement=False)

    star_names = relationship('StarName', back_populates='star')
    star_info_entries = relationship('StarInfoEntry', back_populates='star')

    def __init__(self):
        pass

    def __init__(self, session, obj_id, name):
        self.session = session
        self.id = obj_id
        self.update_names(name)

    def update_names_success_func(self, request):
        lines = request.text.split('\n')
        for l in lines:
            if l[0:2] == '%I':
                name = l[2:]
                if name[0:3] == '.0 ':
                    name = name[3:].strip()
                    main = True
                else:
                    name = name.strip()
                    main = False
                
                # print("* '%s' "%(name), main, end=" ")

                if name not in self.existing_names:
                    star_name = StarName(self.id, name, main)
                    self.existing_names.append(name)
                    try:
                        self.star_names.append(star_name)
                    except UniqueViolation as e:
                        print("unique violation")
                        raise e
                    # print("OK")
                # else:
                #     print("we have that name already")
                
                # add the catalogue if not there yet
                # NOTE: the regular expression seems to be working to prevent formats like BD-NNN   
                alpha = r'A-Za-z'
                num = r'0-9'
                alphanum = r'%s%s'%(alpha, num)
                cat_label = r'[\*%s]+(?:\-[%s][^\ ]*)?'%(alphanum, alpha)
                catalog_designator = r'%s|\[%s\]'%(cat_label, cat_label)
                m = re.match(r'^(%s)(.*)$'%(catalog_designator), name)

                if m:
                    cat, identifier = m.groups()
                    # there should be only one
                    known_cat = self.session.query(Catalog).filter_by(cat_id=cat).first()
                    if not known_cat:
                        print("adding catalog '%s' (name '%s')"%(cat, name))
                        catalog_entry = Catalog()
                        catalog_entry.cat_id = cat
                        self.session.add(catalog_entry)
                else:
                    print('unable to parse catalog identifier')

    def update_names(self, name=None):

        # create all appropriate StarName records with 
        if not name:
            # find main name
            q = self.session.query(StarName).filter_by(star_id=self.id, main=True).all()
            if q:
                star_name = q[0]
                print("found -> ", star_name.star_id, "'%s'"%(star_name.name), star_name.main)
                name = star_name.name
            else:
                print("FATAL: update_names : missing name")
        
        self.existing_names = []
        for star_name in self.star_names:
            if star_name.name not in self.existing_names:
                self.existing_names.append(star_name.name)

        print("search sesame with name '%s' of type %s " % (name, type(name)) )
        url_encoded_name = urllib.parse.quote(name)
        url = 'http://cdsweb.u-strasbg.fr/cgi-bin/nph-sesame/-oI?%s'%(url_encoded_name)
        execute_http_request(url, self.update_names_success_func)

class StarName(Base):

    __tablename__ = 'star_name'

    star_id = Column(Integer, ForeignKey('star.id'))
    name = Column(String(64), primary_key=True)
    main = Column(Boolean)
    
    star = relationship('Star', back_populates='star_names')

    def __init__(self, obj_id, name, main=False):
        self.star_id = obj_id
        self.name = name
        self.main = main

    @staticmethod
    def parse_star_name(session, name):
        print("parse_name")
        name = name.strip()
        
        pos = 0
        while True:
            cat = name[0:pos+1]

            cat_count = session.query(Catalog).filter(Catalog.cat_id.like('%s%%'%(cat))).count()

            print("'%s%%' -> %d catalogs found"%(cat, cat_count))

            if cat_count == 0:
                # we may have overshot
                cat = name[0:pos]

                cat_count = session.query(Catalog).filter(Catalog.cat_id==cat).count()

                print("'%s' -> %d catalogs found"%(cat, cat_count))

                if cat_count == 1:
                    # indeed... adjust pos
                    pos = len(cat)-1
                    break

                # can't find catalogue
                return None

            if cat_count == 1:
                # found only one, 
                # as we may have matched before the end of the catalog name,
                # get the actual catalog name
                catalog = session.query(Catalog).filter(Catalog.cat_id.like('%s%%'%(cat))).first()
                cat = catalog.cat_id
                # adjust pos
                pos = len(cat)-1
                break
            
            # found more than one, increase prefix length
            pos += 1     

        # split catalog_id and identifier 
        # note, remove extraneous space

        SEARCH_ALGS = [
            '%s %s'%(cat, name[pos+1:].strip()),
            '%s %s'%(cat, name[pos+1:].replace(' ', '')),
            '%s%s'%(cat, name[pos+1:].strip()),
            '%s%s'%(cat, name[pos+1:].replace(' ', '')),
        ]

        for search_string in SEARCH_ALGS:
            print("searching for name '%s'"%(search_string))
            sn = session.query(StarName).filter_by(name=search_string).first()
            if sn:
                identifier = (sn.star_id, sn.name, )
                print("found", identifier)
                return identifier   

        print("out of ideas")
        # out of ideas

        return None

    @staticmethod
    def lookup_name(session, name):
        name = name.strip()

        SEARCH_ALGS = [
            [ '%s'%(name), 'like' ],
            [ '* %s'%(name), 'like' ],
            [ '* %s'%(name).lower(), 'ilike' ],
            [ 'V* %s'%(name), 'like' ]
            # comment since it can cause confusion [ 'V* %s'%(name).lower(), 'ilike' ]
            # fault case was for mu GEM that differs from v* mu gem
        ]

        for search in SEARCH_ALGS:
            search_string, mode = search
            if mode =='like':
                sn = session.query(StarName).filter(StarName.name.like(search_string)).first()
                print("looking for name '%s' -> '%s'" % (search_string, repr(sn)))
            if mode == 'ilike':
                sn = session.query(StarName).filter(StarName.name.ilike(search_string)).first()
                print("looking for name '%s' -> '%s'" % (search_string, repr(sn)))
            if sn:
                return (sn.star_id, sn.name, )

        # hell, nothing found, attempt to find parsing the name
        sn = StarName.parse_star_name(session, name)
        print(sn)
        return sn

    @staticmethod
    def cleanup_name_success_func(request):
        oid = None
        canon_name = None
        lines = request.text.split('\n')
        for l in lines:
            if l[0:3] == "%@ ":
                oid = l[3:].strip()
                if oid[0]=='@':
                    oid = oid[1:]
                if not oid.isdecimal():
                    print('invalid OID %s, wrong format, expected \'@\\d*\' line=\'%s\''%(oid, l))
                    oid = None
                else:
                    oid = int(oid)
            elif l[0:5] == "%I.0 ":
                canon_name = l[5:].strip()
        if oid and canon_name:
            identifier = (oid, canon_name,)
            print("cleanup_name found", identifier)
            return identifier
        if not oid:
            print("unable to find oid")
        if not canon_name:
            print("unable to find canonical name")
        # successfully failed
        return None

    @staticmethod
    def cleanup_name(name):
        """
        gets the canon name from the sesame service
        """
        print("searching in Sesame for '%s'"%(name))
        url_encoded_name = urllib.parse.quote(name)
        # loop until this works
        url = 'http://cdsweb.u-strasbg.fr/cgi-bin/nph-sesame?%s'%(url_encoded_name)
        return  execute_http_request(url, StarName.cleanup_name_success_func)


class Band():

    # note: band numbers start at 1
    #
    # current values give the following:
    # code  band  start       end
    # U     1     0.301       0.367 
    # B     2     0.421       0.50275       // overlap with V 
    # V     3     0.50075     0.61125       // overlap with R
    # R     4     0.609245    0.715755      // overlap with I
    # I     5     0.713745    1.025505      // overlap with J
    # J     6     1.0235      1.4495        // overlap with H
    # H     7     1.4475      1.91175       // overlap with K
    # K     8     1.909625    2.821625      // overlap with L
    # L     9     2.8         4.2 
    # M     10    4.2         8.0 
    # N     11    8.0         13.0 
    # Q     12    14.55       18.6 

    BAND_INFO = [
        { 'code': ['U'],           'center': 0.334,    'width': 0.066,     'log_flux_zero': -1.4,      'strehl_max': 0.3,      'name': 'Ultra Violet' },
        { 'code': ['B'],           'center': 0.461875, 'width': 0.08175,   'log_flux_zero': -1.2,      'strehl_max': 0.48,     'name': 'Visible (Blue)' },
        { 'code': ['V'],           'center': 0.556,    'width': 0.1105,    'log_flux_zero': -1.44,     'strehl_max': 0.5,      'name': 'Visible (Green)' },
        { 'code': ['R', 'Halpha'], 'center': 0.6625,   'width': 0.10651,   'log_flux_zero': -1.65,     'strehl_max': 0.65,     'name': 'Visible (Red)' },
        { 'code': ['I'],           'center': 0.869625, 'width': 0.31176,   'log_flux_zero': -1.94,     'strehl_max': 0.75,     'name': 'Near Infrared 1 (I)' },
        { 'code': ['J'],           'center': 1.2365,   'width': 0.426,     'log_flux_zero': -2.5,      'strehl_max': 0.77,     'name': 'Near Infrared 2 (J)' },
        { 'code': ['H', "H'"],     'center': 1.679625, 'width': 0.46425,   'log_flux_zero': -2.94,     'strehl_max': 0.84,     'name': 'Near Infrared 3 (H)' }, 
        { 'code': ['K', "K'"],     'center': 2.365625, 'width': 0.912,     'log_flux_zero': -3.4,      'strehl_max': 0.93,     'name': 'Near Infrared 4 (K)' },
        # L - previous values: 'center': 3.45875, 'width': 1.2785, 'log_flux_zero': -4.15, 'strehl_max': 0.972
        { 'code': ['L'],           'center': 3.5,      'width': 1.4,       'log_flux_zero': -4.154,    'strehl_max': 0.972,    'name': 'Near Infrared 5 (L - MATISSE [2.8 - 4.2])' },
        # M - previous values: 'center': 6.4035, 'width': 4.615, 'log_flux_zero': -4.69, 'strehl_max': 0.985
        { 'code': ['M'],           'center': 6.1,      'width': 3.8,       'log_flux_zero': -4.568,    'strehl_max': 0.985,    'name': 'Mid Infrared 1 (M - MATISSE [4.2 - 8])' },
        # N - previous values: 'center': 11.63, 'width': 5.842, 'log_flux_zero': -5.91, 'strehl_max': 0.996
        { 'code': ['N'],           'center': 10.5,     'width': 5.0,       'log_flux_zero': -6.0,      'strehl_max': 0.996,    'name': 'Mid Infrared 2 (N - MATISSE [8 -13])' },
        { 'code': ['Q'],           'center': 16.575,   'width': 4.05,      'log_flux_zero': -7.17,     'strehl_max': 0.999,    'name': 'Mid Infrared 3'}
    ]

    @staticmethod
    def map_band( wavelength):
        """
        maps lambda (in micrometers) to one of the known band
        
        this is a port of the algorithm in Band.java
        """
        print('mapping band', wavelength)
        # 1) basic test
        nb_bands = len(Band.BAND_INFO)
        max_band_index = nb_bands - 1
        bands = list(range(max_band_index, -1, -1))
        print("running on bands %s"%(repr(bands)))
        for band_code in bands:
            print(band_code, end=' ')
            band_data = Band.BAND_INFO[band_code]
            center_wavelength = band_data['center']
            bandwidth = band_data['width']

            print("checking band %d %s %f %f"%(band_code, str(band_data['code']), center_wavelength, bandwidth))

            # normal test, we're within the band
            if abs(wavelength - center_wavelength) <= bandwidth / 2.0:
                return band_code+1
            # we are under the first band
            if band_code == 0 and wavelength <= center_wavelength:
                return band_code+1
            # we are above the last band
            if band_code == max_band_index and wavelength >= center_wavelength:
                return band_code+1
            elif center_wavelength <= wavelength and wavelength <= Band.BAND_INFO[band_code+1]['center']:
                # were somewhere between 2 bands
                # try between band n and band n+1,
                # use the band_code with smallest distance to center wavelength
                if abs(wavelength - center_wavelength) <= abs(Band.BAND_INFO[band_code+1]['center'] - wavelength):
                    return band_code+1
                else:
                    return band_code+2
        # unable to find bandcode        
        # should not happen
        return None

    @staticmethod
    def calc_wavelength_band_code(value):
        value = float(value)
        if value < 0:
            return { 'error': 'wavelength can\'t be a negative value' }
        band_code = Band.map_band(float(value))
        if not band_code:
            return { 'error': 'wavelength not in known bands' } 
        print('bandcode', band_code, Band.BAND_INFO[band_code-1]['code'])   
        return band_code

    @staticmethod
    def gen_numeric_band_code_list():
        return list(range(1,len(Band.BAND_INFO)+1))

    @staticmethod
    def gen_letter_band_code_list():
        bands = ''
        for band in Band.BAND_INFO:
            code = band['code'][0]
            bands+=code
        return bands

    @staticmethod
    def calc_letter_band_code(value):
        for band_code in range(len(Band.BAND_INFO)):
            band = Band.BAND_INFO[band_code]
            code = band['code']
            # we may have lowecase values ?
            if value in code or value.upper() in code:   
                return band_code+1
        return None

    @staticmethod
    def parse_band_code(band):
        # implies we have a band value
        # the following forms have been seen:

        # [letter code][space]Band
        # [letter code] or [letter code]
        # [letter code] or [letter code] ?
        # y=(one of the above)
        band_letters = Band.gen_letter_band_code_list()
        str_bandcode = r'[%s%s](?:\'|alpha)?'%(band_letters, band_letters.lower())
        str_float = r'([\+-]?\d*(?:\.\d*)?)'
        str_unit = r'(A|mu|nm|μm|um)'
        str_range = r'\ *(?:-|to|\.\.)\ *'
        
        bandcode_re = re.compile(r'^%s$'%(str_bandcode))
        float_re = re.compile(r'^%s$'%(str_float))
        float_unit_re = re.compile(r'^%s\ *?%s$'%(str_float, str_unit))
        range_re = re.compile(r'^%s%s%s\ *?%s?'%(str_float, str_range, str_float, str_unit))
        if band:
            
            # [letter code]
            # [letter code]?
            # [letter code lower case]
            # [letter code]'
            # [letter code]alpha
            m = bandcode_re.match(band)
            if m:
                print('simple letter band \'%s\''%(band))
                value = Band.calc_letter_band_code(band)
                # this should not fail
                if value:
                    return value
                print("FATAL: band out of letter band list - SHOULD NOT HAPPEN")
                return None

            # d
            # d.ddd
            m = float_re.match(band)
            if m:
                print('float only band \'%s\' (assume micrometers)'%(band))
                # we have a numerical value, just a number
                # assume unit is micrometers
                value = m.groups()[0]
                return Band.calc_wavelength_band_code(value)
            
            # ddddA
            # d.dddnm
            # d.ddd mu
            m = float_unit_re.match(band)
            if m:
                value, unit = m.groups()
                print('value', value, 'unit', unit)
                value = float(value)
                
                corrected_unit = 'μm'

                if unit == 'A':
                    actual_unit = 'Å'
                    factor = 10000.0
                elif unit == 'nm':
                    actual_unit = 'nm'
                    factor = 1000.0
                else:
                    actual_unit = 'μm'
                    factor = 1
                    
                actual_value = value/factor
                msg = 'found value \'%f %s (%s)\' -> \'%f %s\' (factor %f)'%(value, unit, actual_unit, actual_value, corrected_unit, factor)
                print('float with unit %s'%(msg))
                bc = Band.calc_wavelength_band_code(actual_value)
                if isinstance(bc, dict):
                    bc['warning'] = msg
                else:
                    bc = { 'band_code': bc, 'warning': msg}    
                return bc

            # d.ddd-d.ddd
            # d.ddd to d.ddd
            # d.ddd..d.ddd[unit]
            m = range_re.match(band)
            if m:
                msg = 'found range %s'%(repr(m.groups()))
                print(msg)
                return { 'error': msg }

            # not just a number. 
            return { 'error': 'unable to parse band value \'%s\''%(band) }  
        # else band is empty
        else:
            return { 'warning': 'band not specified' }
        return None
