from datetime import datetime
import os

from pyramid.config import Configurator
from pyramid.session import SignedCookieSessionFactory


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    with Configurator(settings=settings) as config:
        secret = config.registry.settings.get('session.secret', None)
        if not secret:
            print("Unable to read 'session.secret' from configurtion file, exiting")
            return None
        session_factory = SignedCookieSessionFactory(secret)
        config.set_session_factory(session_factory)

        # Startup timestamp
        date_now = str(datetime.now())
        config.registry.settings['jmdc.start'] = date_now

        # Startup version
        try:
            v = os.environ['IMAGE_TAG']
        except:
            v="IMAGE_TAG undefined"
        config.registry.settings['jmdc.version'] = v

        config.include('pyramid_mailer')
        config.include('.models')
        config.include('pyramid_jinja2')
        config.include('.routes')
        config.scan()
    return config.make_wsgi_app()
