"""first update

Revision ID: 59e01862a009
Revises: 
Create Date: 2019-07-24 12:04:47.744393

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '59e01862a009'
down_revision = None
branch_labels = None
depends_on = None

def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('catalog',
    sa.Column('cat_id', sa.String(), nullable=False),
    sa.Column('name_format', sa.String(), nullable=True),
    sa.PrimaryKeyConstraint('cat_id', name=op.f('pk_catalog'))
    )
    op.create_table('star',
    sa.Column('id', sa.Integer(), autoincrement=False, nullable=False),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_star'))
    )
    op.create_table('star_info',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('id1', sa.String(length=32), nullable=True),
    sa.Column('id2', sa.String(length=32), nullable=True),
    sa.Column('ud_diam', sa.Float(), nullable=True),
    sa.Column('ld_diam', sa.Float(), nullable=True),
    sa.Column('e_ld_diam', sa.Float(), nullable=True),
    sa.Column('band', sa.String(length=10), nullable=True),
    sa.Column('mu_lambda', sa.Float(), nullable=True),
    sa.Column('method', sa.Integer(), nullable=True),
    sa.Column('band_code', sa.Integer(), nullable=True),
    sa.Column('notes', sa.String(length=255), nullable=True),
    sa.Column('bibcode', sa.String(length=19), nullable=True),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_star_info'))
    )
    op.create_table('submission',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('create_ts', sa.TIMESTAMP(), nullable=True),
    sa.Column('email', sa.Text(), nullable=True),
    sa.Column('parse_mode', sa.Integer(), nullable=True),
    sa.Column('headers', sa.Text(), nullable=True),
    sa.Column('upload_ticket', sa.Text(), nullable=True),
    sa.Column('verified', sa.Boolean(), nullable=True),
    sa.Column('moderate_ticket', sa.Text(), nullable=True),
    sa.Column('validated', sa.Boolean(), nullable=True),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_submission'))
    )
    op.create_table('star_info_entry',
    sa.Column('submission_id', sa.Integer(), nullable=False),
    sa.Column('line_number', sa.Integer(), nullable=False),
    sa.Column('line_data', sa.Text(), nullable=True),
    sa.Column('star_info_id', sa.Integer(), nullable=True),
    sa.Column('star_id', sa.Integer(), nullable=True),
    sa.Column('validated', sa.Boolean(), nullable=True),
    sa.Column('comment', sa.Text(), nullable=True),
    sa.ForeignKeyConstraint(['star_id'], ['star.id'], name=op.f('fk_star_info_entry_star_id_star')),
    sa.ForeignKeyConstraint(['star_info_id'], ['star_info.id'], name=op.f('fk_star_info_entry_star_info_id_star_info')),
    sa.ForeignKeyConstraint(['submission_id'], ['submission.id'], name=op.f('fk_star_info_entry_submission_id_submission')),
    sa.PrimaryKeyConstraint('submission_id', 'line_number', name=op.f('pk_star_info_entry')),
    sa.UniqueConstraint('star_info_id', name=op.f('uq_star_info_entry_star_info_id'))
    )
    op.create_table('star_name',
    sa.Column('star_id', sa.Integer(), nullable=True),
    sa.Column('name', sa.String(length=64), nullable=False),
    sa.Column('main', sa.Boolean(), nullable=True),
    sa.ForeignKeyConstraint(['star_id'], ['star.id'], name=op.f('fk_star_name_star_id_star')),
    sa.PrimaryKeyConstraint('name', name=op.f('pk_star_name'))
    )
    # ### end Alembic commands ###

def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('star_name')
    op.drop_table('star_info_entry')
    op.drop_table('submission')
    op.drop_table('star_info')
    op.drop_table('star')
    op.drop_table('catalog')
    # ### end Alembic commands ###
