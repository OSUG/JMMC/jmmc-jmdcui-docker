# jmmc-jmdcui-docker

-> jmmc hey, it's a JMMC product !

-> jmdcui ok the project name may change

-> docker yes we try to avoid speaking on machines even virtual (trying to fly over orchestration with K8S initiative @ OSUG)


# Sprint project dedicated to setup a Web interface to feed collaborativelly the JMDC http://www.jmmc.fr/catalogue_jmdc.htm
 see issue #5
 
## Backlog of futur enhancements
### accept comment (stars) from any users
### add a specific form for a single record more user friendly
### provide a VO TAP compliant interface 

## Technical points

Some [discusion and try have been perform](#1) before coding. 
Our choice are:
- Content display with Datatable 
- Target name check performed with sesame basic url instead of https://astroquery.readthedocs.io/en/latest/simbad/simbad.html
- Framework is pyramid ( the same used by SSHADE / Philipe B. )

### Setup deployement uses docker

[First milestone aims to build and run prototyping container](#2)

The postgres DB is on top of existing container ready for VO TAP constraints (ready for VO interface identified in backlog).
Note: Some enhancement have been operated for dachs.sh



#### Quick collaboration diagram
```mermaid
graph TD

    webapp --> sgbd
    tapserver --> sgbd
    
    subgraph jmdc-ui container 
        webapp
    end
    
    subgraph jmdc-postgres container
        sgbd(Postgresql + PGShere)
    end
    
    subgraph TBD jmdc-tap container 
        tapserver
    end
```


#### How to release a new version ?

- `source .env && git tag -a $IMAGE_TAG -m "v$IMAGE_TAG" && git push --tags`



**Note:** try to follow our [docker guidelines](https://gricad-gitlab.univ-grenoble-alpes.fr/continuous-everything/dockerfile-for-prod) ( and maybe https://codefresh.io/containers/docker-anti-patterns/ )
